module gitlab.com/mergetb/facilities/example

go 1.14

require (
	github.com/fatih/color v1.9.0 // indirect
	gitlab.com/mergetb/xir v0.2.18
)
