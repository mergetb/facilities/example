package main

import (
	"gitlab.com/mergetb/facilities/example/miniclos"
	"gitlab.com/mergetb/xir/lang/go/v0.2/build"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

func main() {
	build.Run(tb.ToXir(miniclos.Topo(), "miniclos"))
}
