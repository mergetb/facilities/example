package miniclos

import (
	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/hw"
	"gitlab.com/mergetb/xir/lang/go/v0.2/sys"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

type Testbed struct {
	Gateway *tb.Resource
	Cmdr    *tb.Resource
	Stor    []*tb.Resource
	XSpine  []*tb.Resource
	XFabric []*tb.Resource
	XLeaf   []*tb.Resource
	ISpine  []*tb.Resource
	IFabric []*tb.Resource
	ILeaf   []*tb.Resource
	Moa     []*tb.Resource
	Servers []*tb.Resource
	Cables  []*hw.Cable
}

func (t *Testbed) Components() ([]*tb.Resource, []*hw.Cable) {

	t.Gateway.Props = xir.Props{
		"name": "gw",
	}

	rs := []*tb.Resource{t.Cmdr, t.Gateway}
	rs = append(rs, t.ISpine...)
	rs = append(rs, t.IFabric...)
	rs = append(rs, t.ILeaf...)
	rs = append(rs, t.XSpine...)
	rs = append(rs, t.XFabric...)
	rs = append(rs, t.XLeaf...)
	rs = append(rs, t.Servers...)
	rs = append(rs, t.Moa...)
	rs = append(rs, t.Stor...)

	return rs, t.Cables

}

const (
	xLeavesPerFabric  = 4
	xNodesPerLeaf     = 4
	xLeavesPerNode    = 2
	xFabricRedundancy = 2
	iNodesPerLeaf     = 4
	iLeavesPerNode    = 1
	iLeavesPerFabric  = 2
	iFabricRedundancy = 2
)

func Topo() *Testbed {

	t := &Testbed{
		// testbed gateway
		Gateway: fabric(
			tb.Gateway,
			tb.Gateway,
			"gw",
			"10.99.0.254",
			3,
			hw.Gbps(100),
			64704,
		),
		Cmdr: cmdr(),

		Moa: []*tb.Resource{
			moa("em0", "10.99.1.20", 67420),
			moa("em1", "10.99.1.21", 67421),
			moa("em2", "10.99.1.22", 67422),
			moa("em3", "10.99.1.23", 67423),
		},

		Stor: []*tb.Resource{
			stor("st0", "10.99.0.20", "172.30.0.3/16", 64720),
			stor("st1", "10.99.0.21", "172.30.0.4/16", 64721),
			stor("st2", "10.99.0.22", "172.30.0.5/16", 64722),
			stor("st3", "10.99.0.23", "172.30.0.6/16", 64723),
		},

		ISpine: []*tb.Resource{
			fabric(tb.InfraSwitch, tb.Spine, "is0", "10.99.0.1", 5, 100, 64701),
			fabric(tb.InfraSwitch, tb.Spine, "is1", "10.99.0.2", 5, 100, 64701),
		},
		IFabric: []*tb.Resource{
			fabric(tb.InfraSwitch, tb.Fabric, "if0", "10.99.0.3", 7, 100, 67403),
			fabric(tb.InfraSwitch, tb.Fabric, "if1", "10.99.0.4", 7, 100, 64704),
			fabric(tb.InfraSwitch, tb.Fabric, "if2", "10.99.0.5", 7, 100, 64705),
			fabric(tb.InfraSwitch, tb.Fabric, "if3", "10.99.0.6", 7, 100, 64706),
		},
		ILeaf: []*tb.Resource{
			leaf(tb.InfraSwitch, "il0", 6, 100),
			leaf(tb.InfraSwitch, "il1", 6, 100),
			leaf(tb.InfraSwitch, "il2", 6, 100),
			leaf(tb.InfraSwitch, "il3", 6, 100),
		},
		XSpine: []*tb.Resource{
			fabric(tb.XpSwitch, tb.Spine, "xs0", "10.99.1.1", 7, 100, 67401),
			fabric(tb.XpSwitch, tb.Spine, "xs1", "10.99.1.2", 7, 100, 64702),
		},
		XFabric: []*tb.Resource{
			fabric(tb.XpSwitch, tb.Fabric, "xf0", "10.99.1.3", 7, 100, 67403),
			fabric(tb.XpSwitch, tb.Fabric, "xf1", "10.99.1.4", 7, 100, 64704),
			fabric(tb.XpSwitch, tb.Fabric, "xf2", "10.99.1.5", 7, 100, 64705),
			fabric(tb.XpSwitch, tb.Fabric, "xf3", "10.99.1.6", 7, 100, 64706),
		},
		XLeaf: []*tb.Resource{
			leaf(tb.XpSwitch, "xl0", 6, 100),
			leaf(tb.XpSwitch, "xl1", 6, 100),
			leaf(tb.XpSwitch, "xl2", 6, 100),
			leaf(tb.XpSwitch, "xl3", 6, 100),
			leaf(tb.XpSwitch, "xl4", 6, 100),
			leaf(tb.XpSwitch, "xl5", 6, 100),
			leaf(tb.XpSwitch, "xl6", 6, 100),
			leaf(tb.XpSwitch, "xl7", 6, 100),
		},
		Servers: []*tb.Resource{
			serverA("n0", imac("70:1A"), xmac("70:1A"), xmac("70:1B")),
			serverA("n1", imac("70:2A"), xmac("70:2A"), xmac("70:2B")),
			serverA("n2", imac("70:3A"), xmac("70:3A"), xmac("70:3C")),
			serverB("n3", imac("70:4A"), xmac("70:4A"), xmac("70:4B")),
			serverA("n4", imac("70:5A"), xmac("70:5A"), xmac("70:5B")),
			serverA("n5", imac("70:6F"), xmac("70:6A"), xmac("70:6B")),
			serverA("n6", imac("70:7A"), xmac("70:7A"), xmac("70:7C")),
			serverB("n7", imac("70:8A"), xmac("70:8A"), xmac("70:8B")),
			serverA("n8", imac("70:9A"), xmac("70:9A"), xmac("70:9B")),
			serverA("n9", imac("70:AF"), xmac("70:AA"), xmac("70:AB")),
			serverA("n10", imac("70:BA"), xmac("70:BA"), xmac("70:BC")),
			serverB("n11", imac("70:CA"), xmac("70:CA"), xmac("70:CB")),
			serverA("n12", imac("70:DA"), xmac("70:DA"), xmac("70:DB")),
			serverA("n13", imac("70:EF"), xmac("70:EA"), xmac("70:EB")),
			serverA("n14", imac("70:FA"), xmac("70:FA"), xmac("70:FC")),
			serverB("n15", imac("70:0A"), xmac("70:0A"), xmac("70:0B")),
		},
	}

	// xpnet

	// xspine - xfabric
	for _, xs := range t.XSpine {
		for _, xf := range t.XFabric {
			t.Cable().Connect(xs.NextSwp(), xf.NextSwp())
		}
	}

	// xfabric - xleaf
	for f, xf := range t.XFabric {
		f /= xFabricRedundancy
		begin := (xLeavesPerFabric) * f
		end := (xLeavesPerFabric) * (f + 1)
		for _, xl := range t.XLeaf[begin:end] {
			t.Cable().Connect(xf.NextSwp(), xl.NextSwp())
		}
	}

	// xleaf - node
	for l, xl := range t.XLeaf {
		p := l % xLeavesPerNode
		l /= xLeavesPerNode
		begin := (xNodesPerLeaf) * l
		end := (xNodesPerLeaf) * (l + 1)
		for _, n := range t.Servers[begin:end] {
			t.Cable().Connect(xl.NextSwp(), n.Eth(1+(p)))
		}
	}

	// server - xleaf

	// infranet

	// ispine - ifabric
	for _, iS := range t.ISpine {
		for _, iF := range t.IFabric {
			t.Cable().Connect(iS.NextSwp(), iF.NextSwp())
		}
	}

	// ifabric - ileaf
	for f, iF := range t.IFabric {

		f /= iFabricRedundancy
		begin := (iLeavesPerFabric) * f
		end := (iLeavesPerFabric) * (f + 1)
		for _, iL := range t.ILeaf[begin:end] {
			t.Cable().Connect(iF.NextSwp(), iL.NextSwp())
		}

	}

	// ileaf - node

	for l, iL := range t.ILeaf {
		p := l % iLeavesPerNode
		l /= iLeavesPerNode
		begin := (iNodesPerLeaf) * l
		end := (iNodesPerLeaf) * (l + 1)
		for _, n := range t.Servers[begin:end] {
			t.Cable().Connect(iL.NextSwp(), n.Eth(0+p))
		}
	}

	// cmdr - gateway
	t.Cable().Connect(t.Gateway.NextSwp(), t.Cmdr.Eth(0))

	// ispine - gateway
	for _, is := range t.ISpine {
		t.Cable().Connect(is.NextSwp(), t.Gateway.NextSwp())
	}

	// xspine - moa
	for i, m := range t.Moa {
		t.Cable().Connect(m.Eth(1), t.XFabric[i].NextSwp())
	}

	// ispine - stor
	for i, s := range t.Stor {
		t.Cable().Connect(s.Eth(1), t.IFabric[i].NextSwp())
	}

	return t

}

// Cable creates a new cable and add it to the testbed
func (t *Testbed) Cable() *hw.Cable {

	cable := hw.GenericCable(hw.Gbps(100))
	t.Cables = append(t.Cables, cable)
	return cable

}

func cmdr() *tb.Resource {

	rs := &tb.Resource{
		Alloc: []tb.AllocMode{tb.NoAlloc},
		Roles: []tb.Role{
			tb.InfraServer,
			tb.EtcdHost,
			tb.MinIOHost,
			tb.RexHost,
			tb.DriverHost,
			tb.ManagerHost,
			tb.CommanderHost,
		},
		System: sys.NewSystem("cmdr", hw.GenericServer(4, hw.Gbps(25))),
		LinkRoles: map[string]tb.Role{
			"eth0": tb.GatewayLink,
			"eth1": tb.InfraLink,
		},
	}

	rs.System.OS.Config = &sys.OsConfig{
		Evpn: &sys.EvpnConfig{
			TunnelIP: "10.99.0.5",
			ASN:      64705,
		},
	}

	return rs

}

func fabric(
	kind, role tb.Role, name, ip string, radix int, gbps uint64, asn int,
) *tb.Resource {

	rs := &tb.Resource{
		Alloc:     []tb.AllocMode{tb.NetAlloc},
		Roles:     []tb.Role{kind, role},
		System:    sys.NewSystem(name, hw.GenericSwitch(radix, hw.Gbps(gbps))),
		LinkRoles: make(map[string]tb.Role),
	}
	rs.System.OS.Config = &sys.OsConfig{
		Evpn: &sys.EvpnConfig{
			TunnelIP: ip,
			ASN:      asn,
		},
		PrimaryBridge: &sys.BridgeConfig{
			Name:      "bridge",
			VlanAware: true,
		},
	}

	rs.System.Device.Model = "Generic Spine Switch"

	if kind == tb.InfraSwitch {
		rs.Alloc = []tb.AllocMode{tb.NoAlloc}
	} else {
		rs.Alloc = []tb.AllocMode{tb.NetAlloc}
		tb.AssignSwpRole(rs, tb.XpLink)
	}

	return rs

}

func leaf(kind tb.Role, name string, radix int, gbps uint64) *tb.Resource {

	rs := &tb.Resource{
		Roles:     []tb.Role{kind, tb.Leaf},
		System:    sys.NewSystem(name, hw.GenericSwitch(radix, hw.Gbps(gbps))),
		LinkRoles: make(map[string]tb.Role),
	}

	rs.System.Device.Model = "Generic Leaf Switch"
	rs.System.OS.Config = &sys.OsConfig{
		PrimaryBridge: &sys.BridgeConfig{
			Name:      "bridge",
			VlanAware: true,
		},
	}

	if kind == tb.InfraSwitch {
		rs.Alloc = []tb.AllocMode{tb.NoAlloc}
	} else {
		rs.Alloc = []tb.AllocMode{tb.NetAlloc}
		tb.AssignSwpRole(rs, tb.XpLink)
	}

	return rs

}

func serverA(name, imac, xmac1, xmac2 string) *tb.Resource {

	s := hw.GenericServer(4, hw.Gbps(10))
	s.Procs = []hw.Proc{
		{Cores: 8},
		{Cores: 8},
	}
	s.Memory = []hw.Dimm{
		{Capacity: hw.Gb(8)},
		{Capacity: hw.Gb(8)},
		{Capacity: hw.Gb(8)},
		{Capacity: hw.Gb(8)},
		{Capacity: hw.Gb(8)},
		{Capacity: hw.Gb(8)},
		{Capacity: hw.Gb(8)},
		{Capacity: hw.Gb(8)},
	}
	s.Nics[0].Ports[1].Mac = imac
	s.Nics[0].Ports[2].Mac = xmac1
	s.Nics[0].Ports[3].Mac = xmac2
	s.Disks = []hw.Disk{
		{Capacity: hw.Tb(1)},
	}

	system := sys.NewSystem(name, s)
	system.OS.Config = &sys.OsConfig{
		Append: "root=UUID=a0000000-0000-0000-0000-00000000000a " +
			"rootfstype=ext4 rw net.ifnames=0 biosdevname=0",
		Rootdev: "sda",
	}

	rs := &tb.Resource{
		Alloc: []tb.AllocMode{
			tb.PhysicalAlloc,
			tb.VirtualAlloc,
		},
		Roles:        []tb.Role{tb.Node},
		System:       system,
		DefaultImage: "debian-buster-disk",
		LinkRoles: map[string]tb.Role{
			"eth0": tb.InfraLink,
			"eth1": tb.XpLink,
			"eth2": tb.XpLink,
		},
	}

	rs.System.Device.Model = "Small Server"

	return rs

}

func serverB(name, imac, xmac1, xmac2 string) *tb.Resource {

	s := hw.GenericServer(4, hw.Gbps(10))
	s.Procs = []hw.Proc{
		{Cores: 6},
		{Cores: 6},
	}
	s.Memory = []hw.Dimm{
		{Capacity: hw.Gb(8)},
		{Capacity: hw.Gb(8)},
		{Capacity: hw.Gb(8)},
		{Capacity: hw.Gb(8)},
	}
	s.Nics[0].Ports[1].Mac = imac
	s.Nics[0].Ports[2].Mac = xmac1
	s.Nics[0].Ports[3].Mac = xmac2
	s.Disks = []hw.Disk{
		{Capacity: hw.Tb(1)},
	}

	system := sys.NewSystem(name, s)
	system.OS.Config = &sys.OsConfig{
		Append: "root=UUID=a0000000-0000-0000-0000-00000000000a " +
			"rootfstype=ext4 rw net.ifnames=0 biosdevname=0",
		Rootdev: "sda",
	}

	rs := &tb.Resource{
		Alloc: []tb.AllocMode{
			tb.PhysicalAlloc,
		},
		Roles:        []tb.Role{tb.Node},
		System:       system,
		DefaultImage: "debian-buster-disk",
		LinkRoles: map[string]tb.Role{
			"eth0": tb.InfraLink,
			"eth1": tb.XpLink,
			"eth2": tb.XpLink,
		},
	}

	rs.System.Device.Model = "Small Server"

	return rs

}

func moa(name, tunnelIP string, asn int) *tb.Resource {

	s := hw.GenericServer(2, hw.Gbps(100))
	system := sys.NewSystem(name, s)
	//system.OS.Links[0].Name = "eth1"
	system.OS.Config = &sys.OsConfig{
		Evpn: &sys.EvpnConfig{
			TunnelIP: tunnelIP,
			ASN:      asn,
		},
	}

	rs := &tb.Resource{
		Alloc:  []tb.AllocMode{tb.NetEmuAlloc},
		Roles:  []tb.Role{tb.NetworkEmulator},
		System: system,
		LinkRoles: map[string]tb.Role{
			"eth1": tb.EmuLink,
		},
	}

	return rs

}

func stor(name, tunnelIP, harborIP string, asn int) *tb.Resource {

	s := hw.GenericServer(2, hw.Gbps(100))
	system := sys.NewSystem(name, s)
	//system.OS.Links[0].Name = "eth1"
	system.OS.Config = &sys.OsConfig{
		Evpn: &sys.EvpnConfig{
			TunnelIP: tunnelIP,
			ASN:      asn,
		},
	}
	system.OS.Links = append(system.OS.Links, &sys.Link{
		Name: "sledbr",
		Kind: "bridge",
		/*Addrs: []net.IPNet{{
			IP:   net.ParseIP(harborIP),
			Mask: net.CIDRMask(16, 32),
		}},*/
		Addrs: []string{harborIP},
	})

	rs := &tb.Resource{
		Alloc: []tb.AllocMode{tb.NoAlloc},
		Roles: []tb.Role{
			tb.StorageServer,
			tb.SledHost,
		},
		System: system,
		LinkRoles: map[string]tb.Role{
			"eth1":   tb.InfraLink,
			"sledbr": tb.HarborEndpoint,
		},
	}

	return rs

}

func (t *Testbed) Provisional() map[string]map[string]interface{} {

	return map[string]map[string]interface{}{
		"*": {
			"image": []string{
				"debian:10",
				"debian:11",
				"ubuntu:1804",
				"ubuntu:1810",
				"ubuntu:2004",
			},
		},
	}

}

func imac(suffix string) string {
	return "04:70:00:01:" + suffix
}

func xmac(suffix string) string {
	return "04:70:11:01:" + suffix
}
