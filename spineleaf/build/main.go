package main

import (
	"gitlab.com/mergetb/facilities/example/spineleaf"
	"gitlab.com/mergetb/xir/lang/go/v0.2/build"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

func main() {

	testbed := spineleaf.Topo()
	build.Run(tb.ToXir(testbed, "spineleaf"))

}
