package main

import (
	"gitlab.com/mergetb/facilities/example/vte"
	"gitlab.com/mergetb/xir/lang/go/v0.2/build"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

func main() {

	testbed := vte.Topo()
	build.Run(tb.ToXir(testbed, "vte"))

}
