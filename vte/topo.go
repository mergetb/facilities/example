package vte

import (
	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/hw"
	"gitlab.com/mergetb/xir/lang/go/v0.2/sys"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

// Testbed contains the testbed model
type Testbed struct {
	Gateway *tb.Resource
	Cmdr    *tb.Resource
	Stor    []*tb.Resource
	XSpine  *tb.Resource
	ISpine  *tb.Resource
	ILeaf   []*tb.Resource
	XLeaf   []*tb.Resource
	Moa     []*tb.Resource
	Servers []*tb.Resource
	Cables  []*hw.Cable
}

func Topo() *Testbed {

	t := &Testbed{
		// testbed gateway
		Gateway: fabric(tb.Gateway, tb.Gateway, "gw", "10.99.0.254", 2, hw.Gbps(100), 64704),
		Cmdr:    cmdr(),

		Moa: []*tb.Resource{
			moa("emu0", "10.99.1.20", 67420),
			moa("emu1", "10.99.1.21", 67421),
		},

		Stor: []*tb.Resource{
			stor("stor0", "10.99.0.20", "172.30.0.3/16", 64720),
			stor("stor1", "10.99.0.21", "172.30.0.4/16", 64721),
			stor("stor2", "10.99.0.22", "172.30.0.5/16", 64722),
			stor("stor3", "10.99.0.23", "172.30.0.6/16", 64723),
		},

		ISpine: fabric(tb.InfraSwitch, tb.Spine, "ispine", "10.99.0.1", 7, 100, 64701),
		XSpine: fabric(tb.XpSwitch, tb.Spine, "xspine", "10.99.1.1", 7, 100, 67401),
		ILeaf: []*tb.Resource{
			ileaf(tb.InfraSwitch, "ileaf0", 5, 100, "172.30.0.7/16"),
			ileaf(tb.InfraSwitch, "ileaf1", 5, 100, "172.30.0.8/16"),
		},
		XLeaf: []*tb.Resource{
			leaf(tb.XpSwitch, "xleaf0", 5, 100),
			leaf(tb.XpSwitch, "xleaf1", 5, 100),
			leaf(tb.XpSwitch, "xleaf2", 5, 100),
			leaf(tb.XpSwitch, "xleaf3", 5, 100),
		},
		Servers: []*tb.Resource{
			smallServer(
				"n0",
				"04:70:00:01:70:1A",
				"04:70:11:01:70:1A",
				"04:70:11:01:70:2A",
			),
			smallServer(
				"n1",
				"04:70:00:01:70:1B",
				"04:70:11:01:70:1B",
				"04:70:11:01:70:2B",
			),
			mediumServer(
				"n2",
				"04:70:00:01:70:1C",
				"04:70:11:01:70:1C",
				"04:70:11:01:70:2C",
			),
			largeServer(
				"n3",
				"04:70:00:01:70:1D",
				"04:70:11:01:70:1D",
				"04:70:11:01:70:2D",
			),
			smallServer(
				"n4",
				"04:70:00:01:70:1E",
				"04:70:11:01:70:1E",
				"04:70:11:01:70:2E",
			),
			smallServer(
				"n5",
				"04:70:00:01:70:1F",
				"04:70:11:01:70:1F",
				"04:70:11:01:70:2F",
			),
			mediumServer(
				"n6",
				"04:70:00:01:70:11",
				"04:70:11:01:70:11",
				"04:70:11:01:70:21",
			),
			largeServer(
				"n7",
				"04:70:00:01:70:12",
				"04:70:11:01:70:12",
				"04:70:11:01:70:22",
			),
		},
	}

	// cmdr - gateway
	t.Cable().Connect(t.Gateway.Swp(0), t.Cmdr.Eth(1))

	// ispine - gateway
	t.Cable().Connect(t.ISpine.Swp(2), t.Gateway.Swp(1))

	// ileaf -ixspine
	t.Cable().Connect(t.ISpine.Swp(0), t.ILeaf[0].Swp(4))
	t.Cable().Connect(t.ISpine.Swp(1), t.ILeaf[1].Swp(4))

	// xleaf - xspine
	t.Cable().Connect(t.XSpine.Swp(0), t.XLeaf[0].Swp(4))
	t.Cable().Connect(t.XSpine.Swp(1), t.XLeaf[1].Swp(4))
	t.Cable().Connect(t.XSpine.Swp(2), t.XLeaf[2].Swp(4))
	t.Cable().Connect(t.XSpine.Swp(3), t.XLeaf[3].Swp(4))

	// server - ileaf
	t.Cable().Connect(t.Servers[0].Eth(1), t.ILeaf[0].Swp(0))
	t.Cable().Connect(t.Servers[1].Eth(1), t.ILeaf[0].Swp(1))
	t.Cable().Connect(t.Servers[2].Eth(1), t.ILeaf[0].Swp(2))
	t.Cable().Connect(t.Servers[3].Eth(1), t.ILeaf[0].Swp(3))

	t.Cable().Connect(t.Servers[4].Eth(1), t.ILeaf[1].Swp(0))
	t.Cable().Connect(t.Servers[5].Eth(1), t.ILeaf[1].Swp(1))
	t.Cable().Connect(t.Servers[6].Eth(1), t.ILeaf[1].Swp(2))
	t.Cable().Connect(t.Servers[7].Eth(1), t.ILeaf[1].Swp(3))

	// server - xleaf
	t.Cable().Connect(t.Servers[0].Eth(2), t.XLeaf[0].Swp(0))
	t.Cable().Connect(t.Servers[0].Eth(3), t.XLeaf[1].Swp(0))
	t.Cable().Connect(t.Servers[1].Eth(2), t.XLeaf[0].Swp(1))
	t.Cable().Connect(t.Servers[1].Eth(3), t.XLeaf[1].Swp(1))
	t.Cable().Connect(t.Servers[2].Eth(2), t.XLeaf[0].Swp(2))
	t.Cable().Connect(t.Servers[2].Eth(3), t.XLeaf[1].Swp(2))
	t.Cable().Connect(t.Servers[3].Eth(2), t.XLeaf[0].Swp(3))
	t.Cable().Connect(t.Servers[3].Eth(3), t.XLeaf[1].Swp(3))

	t.Cable().Connect(t.Servers[4].Eth(2), t.XLeaf[2].Swp(0))
	t.Cable().Connect(t.Servers[4].Eth(3), t.XLeaf[3].Swp(0))
	t.Cable().Connect(t.Servers[5].Eth(2), t.XLeaf[2].Swp(1))
	t.Cable().Connect(t.Servers[5].Eth(3), t.XLeaf[3].Swp(1))
	t.Cable().Connect(t.Servers[6].Eth(2), t.XLeaf[2].Swp(2))
	t.Cable().Connect(t.Servers[6].Eth(3), t.XLeaf[3].Swp(2))
	t.Cable().Connect(t.Servers[7].Eth(2), t.XLeaf[2].Swp(3))
	t.Cable().Connect(t.Servers[7].Eth(3), t.XLeaf[3].Swp(3))

	// xspine - moa
	t.Cable().Connect(t.Moa[0].Eth(1), t.XSpine.Swp(4))
	t.Cable().Connect(t.Moa[1].Eth(1), t.XSpine.Swp(5))

	// ispine - stor
	t.Cable().Connect(t.Stor[0].Eth(1), t.ISpine.Swp(3))
	t.Cable().Connect(t.Stor[1].Eth(1), t.ISpine.Swp(4))
	t.Cable().Connect(t.Stor[2].Eth(1), t.ISpine.Swp(5))
	t.Cable().Connect(t.Stor[3].Eth(1), t.ISpine.Swp(6))

	return t

}

// Components returns the components in the testebd
func (t *Testbed) Components() ([]*tb.Resource, []*hw.Cable) {

	t.Gateway.Props = xir.Props{
		"name": "gw",
	}

	rs := []*tb.Resource{t.Cmdr, t.XSpine, t.ISpine, t.Gateway}
	rs = append(rs, t.ILeaf...)
	rs = append(rs, t.XLeaf...)
	rs = append(rs, t.Servers...)
	rs = append(rs, t.Moa...)
	rs = append(rs, t.Stor...)

	return rs, t.Cables

}

func (t *Testbed) Provisional() map[string]map[string]interface{} {

	return map[string]map[string]interface{}{
		"*": {
			"image": []string{
				"debian:10",
				"debian:11",
				"ubuntu:1804",
				"ubuntu:1810",
				"ubuntu:2004",
			},
		},
	}

}

// Cable creates a new cable and add it to the testbed
func (t *Testbed) Cable() *hw.Cable {

	cable := hw.GenericCable(hw.Gbps(100))
	t.Cables = append(t.Cables, cable)
	return cable

}

func cmdr() *tb.Resource {

	rs := &tb.Resource{
		Alloc: []tb.AllocMode{tb.NoAlloc},
		Roles: []tb.Role{
			tb.InfraServer,
			tb.InfrapodServer,
			tb.EtcdHost,
			tb.MinIOHost,
			tb.RexHost,
			tb.DriverHost,
			tb.ManagerHost,
			tb.CommanderHost,
		},
		System: sys.NewSystem("cmdr", hw.GenericServer(4, hw.Gbps(25))),
		LinkRoles: map[string]tb.Role{
			"eth0": tb.GatewayLink,
			"eth1": tb.InfraLink,
		},
	}

	rs.System.OS.Config = &sys.OsConfig{
		Evpn: &sys.EvpnConfig{
			TunnelIP: "10.99.0.5",
			ASN:      64705,
		},
	}

	return rs

}

func fabric(
	kind, role tb.Role, name, ip string, radix int, gbps uint64, asn int) *tb.Resource {

	rs := &tb.Resource{
		Alloc:  []tb.AllocMode{tb.NetAlloc},
		Roles:  []tb.Role{tb.Fabric, kind, role},
		System: sys.NewSystem(name, hw.GenericSwitch(radix, hw.Gbps(gbps))),
	}
	rs.System.OS.Config = &sys.OsConfig{
		Evpn: &sys.EvpnConfig{
			TunnelIP: ip,
			ASN:      asn,
		},
		PrimaryBridge: &sys.BridgeConfig{
			Name:      "bridge",
			VlanAware: true,
		},
	}

	rs.System.Device.Model = "Generic Spine Switch"

	if kind == tb.InfraSwitch {
		rs.Alloc = []tb.AllocMode{tb.NoAlloc}
	} else {
		rs.Alloc = []tb.AllocMode{tb.NetAlloc}
		tb.AssignSwpRole(rs, tb.XpLink)
	}

	return rs

}

func ileaf(
	kind tb.Role, name string, radix int, gbps uint64, harborSVIaddr string,
) *tb.Resource {

	rs := leaf(kind, name, radix, gbps)
	rs.LinkRoles = map[string]tb.Role{
		"vlan2": tb.HarborEndpoint,
	}
	rs.System.OS.Links = append(rs.System.OS.Links, &sys.Link{
		Name:  "vlan2",
		Kind:  "vlan",
		Addrs: []string{harborSVIaddr},
	})
	return rs

}

func leaf(kind tb.Role, name string, radix int, gbps uint64) *tb.Resource {

	rs := &tb.Resource{
		Roles:  []tb.Role{kind, tb.Leaf},
		System: sys.NewSystem(name, hw.GenericSwitch(radix, hw.Gbps(gbps))),
	}

	rs.System.Device.Model = "Generic Leaf Switch"
	rs.System.OS.Config = &sys.OsConfig{
		PrimaryBridge: &sys.BridgeConfig{
			Name:      "bridge",
			VlanAware: true,
		},
	}

	if kind == tb.InfraSwitch {
		rs.Alloc = []tb.AllocMode{tb.NoAlloc}
	} else {
		rs.Alloc = []tb.AllocMode{tb.NetAlloc}
		tb.AssignSwpRole(rs, tb.XpLink)
	}

	return rs

}

func smallServer(name, imac, xmac1, xmac2 string) *tb.Resource {

	s := hw.GenericServer(4, hw.Gbps(100))
	s.Procs = []hw.Proc{
		{Cores: 8},
	}
	s.Memory = []hw.Dimm{
		{Capacity: hw.Gb(8)},
		{Capacity: hw.Gb(8)},
		{Capacity: hw.Gb(8)},
		{Capacity: hw.Gb(8)},
	}
	s.Nics[0].Ports[1].Mac = imac
	s.Nics[0].Ports[2].Mac = xmac1
	s.Nics[0].Ports[3].Mac = xmac2

	system := sys.NewSystem(name, s)
	system.OS.Config = &sys.OsConfig{
		Append:  "root=PARTUUID=a0000000-0000-0000-0000-00000000000a rootfstype=ext4 rw net.ifnames=0 biosdevname=0 infranet=eth1",
		Rootdev: "sda",
	}

	rs := &tb.Resource{
		Alloc:        []tb.AllocMode{tb.PhysicalAlloc},
		Roles:        []tb.Role{tb.Node},
		System:       system,
		DefaultImage: "debian-buster-disk",
		LinkRoles: map[string]tb.Role{
			"eth1": tb.InfraLink,
			"eth2": tb.XpLink,
			"eth3": tb.XpLink,
		},
		Ptags: []string{"*"},
	}

	rs.System.Device.Model = "Small Server"

	return rs

}

func mediumServer(name, imac, xmac1, xmac2 string) *tb.Resource {

	s := hw.GenericServer(4, hw.Gbps(100))
	s.Procs = []hw.Proc{
		{Cores: 8},
		{Cores: 8},
	}
	s.Memory = []hw.Dimm{
		{Capacity: hw.Gb(16)},
		{Capacity: hw.Gb(16)},
		{Capacity: hw.Gb(16)},
		{Capacity: hw.Gb(16)},
		{Capacity: hw.Gb(16)},
		{Capacity: hw.Gb(16)},
		{Capacity: hw.Gb(16)},
		{Capacity: hw.Gb(16)},
	}
	s.Nics[0].Ports[1].Mac = imac
	s.Nics[0].Ports[2].Mac = xmac1
	s.Nics[0].Ports[3].Mac = xmac2

	system := sys.NewSystem(name, s)
	system.OS.Config = &sys.OsConfig{
		Append:  "root=PARTUUID=a0000000-0000-0000-0000-00000000000a rootfstype=ext4 rw net.ifnames=0 biosdevname=0 infranet=eth1",
		Rootdev: "sda",
	}

	rs := &tb.Resource{
		Alloc:        []tb.AllocMode{tb.PhysicalAlloc},
		Roles:        []tb.Role{tb.Node},
		System:       system,
		DefaultImage: "debian-buster-disk",
		LinkRoles: map[string]tb.Role{
			"eth1": tb.InfraLink,
			"eth2": tb.XpLink,
			"eth3": tb.XpLink,
		},
		Ptags: []string{"*"},
	}

	rs.System.Device.Model = "Medium Server"

	return rs

}

func largeServer(name, imac, xmac1, xmac2 string) *tb.Resource {

	s := hw.GenericServer(4, hw.Gbps(100))
	s.Procs = []hw.Proc{
		{Cores: 16},
		{Cores: 16},
	}
	s.Memory = []hw.Dimm{
		{Capacity: hw.Gb(32)},
		{Capacity: hw.Gb(32)},
		{Capacity: hw.Gb(32)},
		{Capacity: hw.Gb(32)},
		{Capacity: hw.Gb(32)},
		{Capacity: hw.Gb(32)},
		{Capacity: hw.Gb(32)},
		{Capacity: hw.Gb(32)},
	}
	s.Nics[0].Ports[1].Mac = imac
	s.Nics[0].Ports[2].Mac = xmac1
	s.Nics[0].Ports[3].Mac = xmac2

	system := sys.NewSystem(name, s)
	system.OS.Config = &sys.OsConfig{
		Append:  "root=PARTUUID=a0000000-0000-0000-0000-00000000000a rootfstype=ext4 rw net.ifnames=0 biosdevname=0 infranet=eth1",
		Rootdev: "sda",
	}

	rs := &tb.Resource{
		Alloc: []tb.AllocMode{
			tb.PhysicalAlloc,
			tb.VirtualAlloc,
		},
		Roles:        []tb.Role{tb.Node},
		System:       system,
		DefaultImage: "debian-buster-disk",
		LinkRoles: map[string]tb.Role{
			"eth1": tb.InfraLink,
			"eth2": tb.XpLink,
			"eth3": tb.XpLink,
		},
		Ptags: []string{"*"},
	}

	rs.System.Device.Model = "Large Server"

	return rs

}

func moa(name, tunnelIP string, asn int) *tb.Resource {

	s := hw.GenericServer(2, hw.Gbps(100))
	system := sys.NewSystem(name, s)
	//system.OS.Links[0].Name = "eth1"
	system.OS.Config = &sys.OsConfig{
		Evpn: &sys.EvpnConfig{
			TunnelIP: tunnelIP,
			ASN:      asn,
		},
	}

	rs := &tb.Resource{
		Alloc:  []tb.AllocMode{tb.NetEmuAlloc},
		Roles:  []tb.Role{tb.NetworkEmulator},
		System: system,
		LinkRoles: map[string]tb.Role{
			"eth1": tb.EmuLink,
		},
	}

	return rs

}

func stor(name, tunnelIP, harborIP string, asn int) *tb.Resource {

	s := hw.GenericServer(2, hw.Gbps(100))
	system := sys.NewSystem(name, s)
	//system.OS.Links[0].Name = "eth1"
	system.OS.Config = &sys.OsConfig{
		Evpn: &sys.EvpnConfig{
			TunnelIP: tunnelIP,
			ASN:      asn,
		},
	}
	system.OS.Links = append(system.OS.Links, &sys.Link{
		Name: "sledbr",
		Kind: "bridge",
		/*Addrs: []net.IPNet{{
			IP:   net.ParseIP(harborIP),
			Mask: net.CIDRMask(16, 32),
		}},*/
		Addrs: []string{harborIP},
	})

	rs := &tb.Resource{
		Alloc: []tb.AllocMode{tb.NoAlloc},
		Roles: []tb.Role{
			tb.StorageServer,
			tb.SledHost,
		},
		System: system,
		LinkRoles: map[string]tb.Role{
			"eth1":   tb.InfraLink,
			"sledbr": tb.HarborEndpoint,
		},
	}

	return rs

}
